﻿using System;
namespace ATM
{
    /// <summary>
    /// Base Class for an Account
    /// </summary>
    public abstract class Account
    {
        protected string name, accountNumber;
        protected double balance;
        public Account(string name,string accountNumber, double balance)
        {
            this.name = name;
            this.accountNumber = accountNumber;
            this.balance = balance;
        }

        /// <summary>
        /// Method used to deposit money into an Account.
        /// </summary>
        /// <param name="amt">Double number</param>
        public abstract void Deposit(double amt);

        /// <summary>
        /// Method used to check the balance in an Account.
        /// </summary>
        /// <returns>Method should return the balance of the Account.</returns>
        public abstract double CheckBalance();

        /// <summary>
        /// Method used to withdraw money from an Account.
        /// </summary>
        /// <param name="amt">Double number</param>
        public abstract void WithDraw(double amt);
    }
}
