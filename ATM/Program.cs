﻿using System;
using System.Data.SqlTypes;
namespace ATM
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to My_ATM!");
            Console.WriteLine("Choose a number to select an option below:");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1. Create a new Account");
            Console.WriteLine("2. Access Existing Account");
            Console.ResetColor();
            Console.Write("Enter Option: ");

            string option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    Console.WriteLine("What Account Would you Like to create");
                    Console.WriteLine("Choose a number to select an option below:");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("1. Create current Account");
                    Console.WriteLine("2. Create Savings Account");
                    Console.ResetColor();
                    
                    string accOption = Console.ReadLine();

                    switch (accOption)
                    {
                        case "1":
                            CreateCurrentAccount();
                            break;

                        case "2":
                            CreateSavingsAccount();
                            break;

                        default:
                            Console.WriteLine("Invalid Option");
                            break;
                    }
                    break;

                case "2":
                    Console.WriteLine("Choose a number to select an option below:");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("1. Create current Account");
                    Console.WriteLine("2. Create Savings Account");
                    Console.ResetColor();
                    break;


                default:
                    Console.WriteLine("Invalid Option");
                    break;
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Method to create a new Current account.
        /// </summary>
        static void CreateCurrentAccount()
        {
            Console.WriteLine("Enter name of the account holder");

            string name = Console.ReadLine();

            Console.WriteLine("Enter starting balance");

            double balance = Convert.ToDouble(Console.ReadLine());
            Random random = new Random();

            new CurrentAccount(name, random.Next(100).ToString(),balance);
        }

        /// <summary>
        /// Method to create a new Savings account.
        /// </summary>
        static void CreateSavingsAccount()
        {
            Console.WriteLine("Enter name of the account holder");

            string name = Console.ReadLine();

            Console.WriteLine("Enter starting balance");

            double balance = Convert.ToDouble(Console.ReadLine());
            new SavingsAccount(name, new Random().Next(100).ToString(), balance);
            
        }

    }
}
