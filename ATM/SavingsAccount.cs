﻿using System;
namespace ATM
{
    public class SavingsAccount:Account
    {
        public SavingsAccount(string name, string accountNumber, double balance):base(name, accountNumber, balance)
        {
            // TODO: Create DAO class that will have a method to create a new table here
            Console.WriteLine("{0} with account number {1} created a new Savings Account", name, accountNumber);
        }

        public override double CheckBalance()
        {
            throw new NotImplementedException();
        }

        public override void Deposit(double amt)
        {
            throw new NotImplementedException();
        }

        public override void WithDraw(double amt)
        {
            throw new NotImplementedException();
        }
    }
}
