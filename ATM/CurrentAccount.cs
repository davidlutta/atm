﻿using System;
namespace ATM
{
    /// <summary>
    /// Contains all the methods for a current account
    /// </summary>
    public class CurrentAccount : Account
    {

        public CurrentAccount(string name, string accountNumber, double balance):base(name, accountNumber, balance)
        {
            // TODO: Create DAO class that will have a method to create a new table here
            Console.WriteLine("{0} with account number {1} registered for a current Account", name, accountNumber);
        }

        public override double CheckBalance()
        {

            throw new NotImplementedException();
        }

        public override void Deposit(double amt)
        {
            throw new NotImplementedException();
        }

        public override void WithDraw(double amt)
        {
            // TODO: Retrieve the balance and create an overdraft limit +20,000
            
            throw new NotImplementedException();
        }
    }
}
